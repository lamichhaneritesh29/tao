msgid ""
msgstr ""
"Project-Id-Version: TAO 3.3.0-sprint96\n"
"PO-Revision-Date: 2019-04-25T01:10:54\n"
"Last-Translator: TAO Translation Team <translation@tao.lu>\n"
"MIME-Version: 1.0\n"
"Language: ca-ES-valencia\n"
"sourceLanguage: en-US\n"
"targetLanguage: ca-ES-valencia\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"

# http://www.tao.lu/Ontologies/TAOItem.rdf#Item
msgctxt "http://www.w3.org/2000/01/rdf-schema#label"
msgid "Item"
msgstr "Element"

# http://www.tao.lu/Ontologies/TAOItem.rdf#Item
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "Item"
msgstr "Element"

# http://www.tao.lu/Ontologies/TAOItem.rdf#ItemContent
msgctxt "http://www.w3.org/2000/01/rdf-schema#label"
msgid "Item Content"
msgstr "Contingut d'element"

# http://www.tao.lu/Ontologies/TAOItem.rdf#ItemContent
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "Item Content"
msgstr "Contingut d'element"

# http://www.tao.lu/Ontologies/TAOItem.rdf#ItemContentSourceName
msgctxt "http://www.w3.org/2000/01/rdf-schema#label"
msgid "Original Filename"
msgstr "Nom de fitxer original"

# http://www.tao.lu/Ontologies/TAOItem.rdf#ItemContentSourceName
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "Original Filename"
msgstr "Nom de fitxer original"

# http://www.tao.lu/Ontologies/TAOItem.rdf#ItemModels
msgctxt "http://www.w3.org/2000/01/rdf-schema#label"
msgid "Item Models"
msgstr "Models d'element"

# http://www.tao.lu/Ontologies/TAOItem.rdf#ItemModels
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "Item Models"
msgstr "Models d'element"

# http://www.tao.lu/Ontologies/TAOItem.rdf#ItemModel
msgctxt "http://www.w3.org/2000/01/rdf-schema#label"
msgid "Item Model"
msgstr "Model d'element"

# http://www.tao.lu/Ontologies/TAOItem.rdf#ItemModel
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "Item Model"
msgstr "Model d'element"

# http://www.tao.lu/Ontologies/TAOItem.rdf#ItemRuntime
msgctxt "http://www.w3.org/2000/01/rdf-schema#label"
msgid "Runtime"
msgstr "Temps d'execució"

# http://www.tao.lu/Ontologies/TAOItem.rdf#ItemRuntime
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "Runtime"
msgstr "Temps d'execució"

# http://www.tao.lu/Ontologies/TAOItem.rdf#ModelService
msgctxt "http://www.w3.org/2000/01/rdf-schema#label"
msgid "itemmodel service"
msgstr "servei de model d'element"

# http://www.tao.lu/Ontologies/TAOItem.rdf#ModelService
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "service of the item model"
msgstr "servei de model d'element"

# http://www.tao.lu/Ontologies/TAOItem.rdf#DataFileName
msgctxt "http://www.w3.org/2000/01/rdf-schema#label"
msgid "Data File Name"
msgstr "Nom del fitxer de dades"

# http://www.tao.lu/Ontologies/TAOItem.rdf#DataFileName
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "the filename used to store the item content for this model"
msgstr "el nom de fitxer utilitzat per a emmagatzemar el contingut de l'element per a aquest model"

# http://www.tao.lu/Ontologies/TAOItem.rdf#ModelStatus
msgctxt "http://www.w3.org/2000/01/rdf-schema#label"
msgid "Model Status"
msgstr "Estat del model"

# http://www.tao.lu/Ontologies/TAOItem.rdf#StatusStable
msgctxt "http://www.w3.org/2000/01/rdf-schema#label"
msgid "Stable"
msgstr "Estable"

# http://www.tao.lu/Ontologies/TAOItem.rdf#StatusStable
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "This item  model can be used in production"
msgstr "Aquest model d'element es pot utilitzar en producció"

# http://www.tao.lu/Ontologies/TAOItem.rdf#StatusDeprecated
msgctxt "http://www.w3.org/2000/01/rdf-schema#label"
msgid "Deprecated"
msgstr "Obsolet"

# http://www.tao.lu/Ontologies/TAOItem.rdf#StatusDeprecated
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "This item model is not supported anymore, we advise to migrate your items"
msgstr "Aquest model d'element ja no rep suport, us recomanem que migreu els vostres elements"

# http://www.tao.lu/Ontologies/TAOItem.rdf#StatusDevelopment
msgctxt "http://www.w3.org/2000/01/rdf-schema#label"
msgid "Development"
msgstr "Desenvolupament"

# http://www.tao.lu/Ontologies/TAOItem.rdf#StatusDevelopment
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "this item model has not been tested in a production environment, we advise to be carefull with it"
msgstr "aquest model d'element no s'ha provat en un entorn de producció, us recomanem que hi aneu amb compte"

# http://www.tao.lu/Ontologies/TAOItem.rdf#StatusExperimental
msgctxt "http://www.w3.org/2000/01/rdf-schema#label"
msgid "Experimental"
msgstr "Experimental"

# http://www.tao.lu/Ontologies/TAOItem.rdf#StatusExperimental
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "this item model is still in conception, but you can try it, give your feedback and it will be improved."
msgstr "aquest model està encara en procés d'elaboració, però podeu provar-lo; si doneu retroacció es podrà millorar"

# http://www.tao.lu/Ontologies/TAOItem.rdf#ItemModelStatus
msgctxt "http://www.w3.org/2000/01/rdf-schema#label"
msgid "Status"
msgstr "Estat"

# http://www.tao.lu/Ontologies/TAOItem.rdf#ItemModelStatus
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "The status of an item model"
msgstr "L'estat d'un model d'element"

# http://www.tao.lu/Ontologies/TAOItem.rdf#ItemsManagerRole
msgctxt "http://www.w3.org/2000/01/rdf-schema#label"
msgid "Items Manager"
msgstr "Gestor d'elements"

# http://www.tao.lu/Ontologies/TAOItem.rdf#ItemsManagerRole
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "The Items Manager Role"
msgstr "Rol de gestor d'elements"

# http://www.tao.lu/Ontologies/TAOItem.rdf#AbstractItemAuthor
msgctxt "http://www.w3.org/2000/01/rdf-schema#label"
msgid "Restricted Item Author"
msgstr "Autor d'element restringit"

# http://www.tao.lu/Ontologies/TAOItem.rdf#AbstractItemAuthor
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "Item authoring only Role"
msgstr "Autoria d'elements només rol"

# http://www.tao.lu/Ontologies/TAOItem.rdf#ItemAuthor
msgctxt "http://www.w3.org/2000/01/rdf-schema#label"
msgid "Item Author"
msgstr "Autor d'elements"

# http://www.tao.lu/Ontologies/TAOItem.rdf#ItemAuthor
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "The Item Author and Manager Role"
msgstr "Rol de gestor i autor d'elements"

