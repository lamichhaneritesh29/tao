msgid ""
msgstr ""
"Project-Id-Version: TAO 3.3.0-sprint96\n"
"PO-Revision-Date: 2019-04-25T01:04:24\n"
"Last-Translator: TAO Translation Team <translation@tao.lu>\n"
"MIME-Version: 1.0\n"
"Language: it-IT\n"
"sourceLanguage: en-US\n"
"targetLanguage: it-IT\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#AssembledDelivery
msgctxt "http://www.w3.org/2000/01/rdf-schema#label"
msgid "Delivery"
msgstr "Somministrazione"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#AssembledDelivery
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "Delivery"
msgstr "Somministrazione"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#CustomLabel
msgctxt "http://www.w3.org/2000/01/rdf-schema#label"
msgid "Title"
msgstr ""

# http://www.tao.lu/Ontologies/TAODelivery.rdf#CustomLabel
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "Label which show for test-takers"
msgstr ""

# http://www.tao.lu/Ontologies/TAODelivery.rdf#PeriodStart
msgctxt "http://www.w3.org/2000/01/rdf-schema#label"
msgid "Start Date"
msgstr "Data di Partenza"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#PeriodStart
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "The start date of the delivery"
msgstr "Data di partenza della somministrazione"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#PeriodEnd
msgctxt "http://www.w3.org/2000/01/rdf-schema#label"
msgid "End Date"
msgstr "Data di Fine"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#PeriodEnd
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "The end date of the delivery"
msgstr "Data di fine della somministrazione"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#Maxexec
msgctxt "http://www.w3.org/2000/01/rdf-schema#label"
msgid "Max. Executions (default: unlimited)"
msgstr ""

# http://www.tao.lu/Ontologies/TAODelivery.rdf#Maxexec
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "Maximum Times of Execution per subject"
msgstr "Tempi Massimi di Esecuzione per soggetto"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#AccessSettings
msgctxt "http://www.w3.org/2000/01/rdf-schema#label"
msgid "Access"
msgstr "Accesso"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#AccessSettings
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "Access"
msgstr "Accesso"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#DisplayOrder
msgctxt "http://www.w3.org/2000/01/rdf-schema#label"
msgid "Display Order"
msgstr ""

# http://www.tao.lu/Ontologies/TAODelivery.rdf#DisplayOrder
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "A weight determining in which order to display the available deliveries to Test Takers. The deliveries will be displayed in ascending order, depending on the integer value of this property."
msgstr ""

# http://www.tao.lu/Ontologies/TAODelivery.rdf#DeliveryTestRunnerFeatures
msgctxt "http://www.w3.org/2000/01/rdf-schema#label"
msgid "Test Runner Features"
msgstr ""

# http://www.tao.lu/Ontologies/TAODelivery.rdf#DeliveryTestRunnerFeatures
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "Test runner features associated to the delivery"
msgstr ""

# http://www.tao.lu/Ontologies/TAODelivery.rdf#ExcludedSubjects
msgctxt "http://www.w3.org/2000/01/rdf-schema#label"
msgid "Excluded Subjects"
msgstr "Soggetti Esclusi"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#ExcludedSubjects
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "Excluded Subjects"
msgstr "Soggetti Esclusi"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#DeliveryResultServer
msgctxt "http://www.w3.org/2000/01/rdf-schema#label"
msgid "Result Server"
msgstr "Server Risultato"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#DeliveryResultServer
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "Result Server"
msgstr "Server Risultato"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#DeliveryPlugins
msgctxt "http://www.w3.org/2000/01/rdf-schema#label"
msgid "Plugins"
msgstr ""

# http://www.tao.lu/Ontologies/TAODelivery.rdf#DeliveryPlugins
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "Plugins associated to the delivery"
msgstr ""

# http://www.tao.lu/Ontologies/TAODelivery.rdf#DeliveryExecution
msgctxt "http://www.w3.org/2000/01/rdf-schema#label"
msgid "Delivery execution"
msgstr "Esecuzione della somministrazione"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#DeliveryExecution
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "Execution of a delivery"
msgstr "Esecuzione di una somministrazione"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#DeliveryExecutionDelivery
msgctxt "http://www.w3.org/2000/01/rdf-schema#label"
msgid "Execution's delivery"
msgstr "Somministrazione dell'esecuzione"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#DeliveryExecutionDelivery
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "Delivery of this execution"
msgstr "Somministrazione di questa esecuzione"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#DeliveryExecutionSubject
msgctxt "http://www.w3.org/2000/01/rdf-schema#label"
msgid "DeliveryExecution Subject"
msgstr "Soggetto dell'Esecuzione della Somministrazione"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#DeliveryExecutionSubject
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "test-taker of the delivery execution"
msgstr "Partecipante dell'esecuzione della somministrazione"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#DeliveryExecutionStart
msgctxt "http://www.w3.org/2000/01/rdf-schema#label"
msgid "DeliveryExecution start"
msgstr "Inizio dell'Esecuzione della Somministrazione"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#DeliveryExecutionStart
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "Timestamp of the delivery execution start"
msgstr "Timestamp dell'inizio dell'esecuzione della somministrazione"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#DeliveryExecutionEnd
msgctxt "http://www.w3.org/2000/01/rdf-schema#label"
msgid "DeliveryExecution end"
msgstr "Fine dell'Esecuzione della Somministrazione"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#DeliveryExecutionEnd
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "Timestamp of the delivery finish"
msgstr "Timestamp del termine della somministrazione"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#StatusOfDeliveryExecution
msgctxt "http://www.w3.org/2000/01/rdf-schema#label"
msgid "DeliveryExecutions Status"
msgstr "Stato dell'Esecuzione della Somministrazione"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#StatusOfDeliveryExecution
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "The current status of the DeliveryExecution"
msgstr "Lo stato corrente della Esecuzione della Somministrazione"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#DeliveryExecutionStatus
msgctxt "http://www.w3.org/2000/01/rdf-schema#label"
msgid "DeliveryExecution status"
msgstr "Stato della Esecuzione della Somministrazione"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#DeliveryExecutionStatus
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "Status of a delivery execution"
msgstr "Stato di una Esecuzione della Somministrazione"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#DeliveryCompileTask
msgctxt "http://www.w3.org/2000/01/rdf-schema#label"
msgid "Delivery compile task"
msgstr ""

# http://www.tao.lu/Ontologies/TAODelivery.rdf#DeliveryCompileTask
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "Delivery compile task id"
msgstr ""

# http://www.tao.lu/Ontologies/TAODelivery.rdf#DeliveryExecutionStatusActive
msgctxt "http://www.w3.org/2000/01/rdf-schema#label"
msgid "Active"
msgstr "Attivo"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#DeliveryExecutionStatusActive
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "Activ status of a delivery execution"
msgstr "Stato attivo di un'esecuzione di somministrazione"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#DeliveryExecutionStatusAbandoned
msgctxt "http://www.w3.org/2000/01/rdf-schema#label"
msgid "Finished"
msgstr "Terminato"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#DeliveryExecutionStatusFinished
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "Finished status of a delivery execution"
msgstr "Stato terminato di un'esecuzione di consegna"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#DeliveryExecutionStatusPaused
msgctxt "http://www.w3.org/2000/01/rdf-schema#label"
msgid "Paused"
msgstr ""

# http://www.tao.lu/Ontologies/TAODelivery.rdf#DeliveryExecutionStatusPaused
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "Paused status of a delivery execution"
msgstr ""

# http://www.tao.lu/Ontologies/TAODelivery.rdf#DeliveryExecutionStatusAbandoned
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "Abandoned status of a delivery execution"
msgstr "Stato abbandonato di un'esecuzione di consegna"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#AssembledDelivery
msgctxt "http://www.w3.org/2000/01/rdf-schema#label"
msgid "Assembled Delivery "
msgstr "Somministrazione assemblata"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#AssembledDelivery
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "An assembled Delivery"
msgstr "Una Somministrazione assemblata"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#AssembledDeliveryCompilationTime
msgctxt "http://www.w3.org/2000/01/rdf-schema#label"
msgid "Compilation Time"
msgstr "Tempo di compilazione"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#AssembledDeliveryCompilationTime
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "The time of the compilation of the delivery"
msgstr "Il tempo della compilazione della somministrazione"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#AssembledDeliveryCompilationDirectory
msgctxt "http://www.w3.org/2000/01/rdf-schema#label"
msgid "Compilation Directory"
msgstr "Directory di compilazione"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#AssembledDeliveryCompilationDirectory
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "The directory of the compilation of the delivery"
msgstr "La directory della compilazione della somministrazione"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#AssembledDeliveryOrigin
msgctxt "http://www.w3.org/2000/01/rdf-schema#label"
msgid "Delivery origin"
msgstr "Origine della somministrazione"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#AssembledDeliveryOrigin
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "The original test/template of the delivery"
msgstr "Il test/modello originale della somministrazione"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#AssembledDeliveryContainer
msgctxt "http://www.w3.org/2000/01/rdf-schema#label"
msgid "Delivery container serial"
msgstr ""

# http://www.tao.lu/Ontologies/TAODelivery.rdf#AssembledDeliveryContainer
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "The serialized delivery container"
msgstr ""

# http://www.tao.lu/Ontologies/TAODelivery.rdf#AssembledDeliveryRuntime
msgctxt "http://www.w3.org/2000/01/rdf-schema#label"
msgid "Runtime"
msgstr "Tempo di esecuzione"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#AssembledDeliveryRuntime
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "Service call to run the compiled delivery"
msgstr "Chiamata di servizio per eseguire la somministrazione compilata"

# http://www.tao.lu/Ontologies/TAOGroup.rdf#Deliveries
msgctxt "http://www.w3.org/2000/01/rdf-schema#label"
msgid "Deliveries"
msgstr "Somministrazioni"

# http://www.tao.lu/Ontologies/TAOGroup.rdf#Deliveries
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "Deliveries of the group"
msgstr "Somministrazioni del gruppo"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#DeliveryManagerRole
msgctxt "http://www.w3.org/2000/01/rdf-schema#label"
msgid "Delivery Manager"
msgstr "Gestione Somministrazione"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#DeliveryManagerRole
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "The Delivery Manager Role"
msgstr "Il Ruolo Gestione Somministrazione"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#GuestAccess
msgctxt "http://www.w3.org/2000/01/rdf-schema#label"
msgid "Guest Access"
msgstr "Accesso Ospite"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#GuestAccess
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "Guest Access"
msgstr "Accesso Ospite"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#Maxexec
msgctxt "http://www.w3.org/2000/01/rdf-schema#label"
msgid "Max. number of executions (default: unlimited)"
msgstr "Max. numero di esecuzioni (default: Illimitate)"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#PeriodStart
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "Start Date"
msgstr "Data di Inizio"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#PeriodEnd
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "End Date"
msgstr "Data di Termine"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#Maxexec
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "Max. number of executions (default: unlimited)"
msgstr "Max. numero di esecuzioni (default: illimitate)"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#AbstractDeliveryContent
msgctxt "http://www.w3.org/2000/01/rdf-schema#label"
msgid "Delivery Content abstraction"
msgstr "Astrazione del Contenuto di Somministrazione"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#AbstractDeliveryContent
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "Delivery Content abstraction"
msgstr "Astrazione del Contenuto di Somministrazione"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#DeliveryContentImplementation
msgctxt "http://www.w3.org/2000/01/rdf-schema#label"
msgid "DeliveryContent implementation"
msgstr "Implementazione del Contenuto di Somministrazione"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#DeliveryContentImplementation
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "DeliveryContent implementation"
msgstr "Implementazione del Contenuto di Somministrazione"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#DeliveryContent
msgctxt "http://www.w3.org/2000/01/rdf-schema#label"
msgid "Delivery Content"
msgstr "Contenuto di Somministrazione"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#DeliveryContent
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "Delivery Content"
msgstr "Contenuto di Somministrazione"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#DeliveryExecution
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "Delivery execution"
msgstr "Esecuzione della Somministrazione"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#DeliveryExecutionDelivery
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "Execution's delivery"
msgstr "Somministrazione di questa Esecuzione"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#DeliveryExecutionSubject
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "DeliveryExecution Subject"
msgstr "Soggetto dell'Esecuzione della Somministrazione"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#DeliveryExecutionStart
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "DeliveryExecution start"
msgstr "Inizio dell'Esecuzione della Somministrazione"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#DeliveryExecutionEnd
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "DeliveryExecution end"
msgstr "Fine dell'Esecuzione della Somministrazione"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#StatusOfDeliveryExecution
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "DeliveryExecutions Status"
msgstr "Stato dell'Esecuzione della Somministrazione"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#DeliveryExecutionStatus
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "DeliveryExecution status"
msgstr "Stato dell'Esecuzione della Somministrazione"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#DeliveryExecutionStatusActive
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "Active"
msgstr "Attivo"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#DeliveryExecutionStatusAbandoned
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "Finished"
msgstr "Terminato"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#AssembledDeliveryCompilationTime
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "Compilation Time"
msgstr "Tempo di Compilazione"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#AssembledDeliveryCompilationDirectory
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "Compilation Directory"
msgstr "Directory di Compilazione"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#AssembledDeliveryRuntime
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "Runtime"
msgstr "Tempo di esecuzione"

# http://www.tao.lu/Ontologies/TAOGroup.rdf#Deliveries
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "Deliveries"
msgstr "Somministrazioni"

# http://www.tao.lu/Ontologies/TAODelivery.rdf#DeliveryManagerRole
msgctxt "http://www.w3.org/2000/01/rdf-schema#comment"
msgid "Delivery Manager"
msgstr "Gestione Somministrazione"

